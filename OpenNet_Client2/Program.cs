﻿using libOpenNet.src;
using libOpenNet.src.Client;
using libOpenNet.src.SharedObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OpenNet_Client2
{
    class Program
    {
        static string ConfigPath = "./config.xml";
        static void Main(string[] args)
        {
            Console.Title = "Client 2";
            Config config = Config.LoadConfig(ConfigPath);

            NetworkClient client = new NetworkClient(ConfigPath);


            Shared_RelayInfo well = client.SharedRelay.GetRelayInfo();
            config.AddRelay(client, well);

            foreach (Shared_RelayInfo relay in client.SharedRelay.GetAllRelayInfo())
            {
                config.AddRelay(client, relay);
            }


            //config.AddService(22, "127.0.0.1", 22);

            Console.WriteLine($"Connected to: {client.ConnectedProperty.HostIp}:{client.ConnectedProperty.Port}");

            //connect to a default debug/test client
            //the Hostname below "r9nom1hfeahawizcpuxoyyfomytkkmko8x92tmjg" is the very first service hosted in this Darknet project
            string TargetClientHostName = "r9nom1hfeahawizcpuxoyyfomytkkmko8x92tmjg";

            Thread.Sleep(1000);
            client.ConnectToService(TargetClientHostName);

            Console.ReadLine();
        }
    }
}
