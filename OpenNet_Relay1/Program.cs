﻿using libOpenNet.src;
using libOpenNet.src.Client;
using libOpenNet.src.Relay;
using libOpenNet.src.SharedObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenNet_Relay1
{
    class Program
    {
        static string ConfigPath = "./config.xml";

        static void Main(string[] args)
        {
            Console.Title = "Relay1";
            Config config = Config.LoadConfig(ConfigPath);

            RelayServer relay = new RelayServer(ConfigPath);

            Console.WriteLine("Started Relay1");
            Console.ReadLine();
        }
    }
}
