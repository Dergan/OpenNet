﻿using libOpenNet.src;
using libOpenNet.src.Client;
using libOpenNet.src.SharedObjects;
using SecureSocketProtocol3.Security.Encryptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenNet_Client
{
    class Program
    {
        static string ConfigPath = "./config.xml";

        static void Main(string[] args)
        {
            Console.Title = "Client";
            Config config = Config.LoadConfig(ConfigPath);

            NetworkClient client = new NetworkClient(ConfigPath);


            Shared_RelayInfo well = client.SharedRelay.GetRelayInfo();
            config.AddRelay(client, well);

            foreach (Shared_RelayInfo relay in client.SharedRelay.GetAllRelayInfo())
            {
                config.AddRelay(client, relay);
            }

            if (config.Services == null || config.Services.Count == 0)
            {
                config.AddService(22, "127.0.0.1", 22);
                Console.WriteLine("Added service 22:127.0.0.1:22");
            }

            Console.WriteLine($"Connected to: {client.ConnectedProperty.HostIp}:{client.ConnectedProperty.Port}");

            if (config.Services.Count > 0)
            {
                Shared_ServiceInfo serviceInfo = new Shared_ServiceInfo();
                serviceInfo.PublicKey = new RSAEncryption(4096, config.PrivateKey).PublicParameters.Value.Modulus;
                serviceInfo.RelayEntryPoints = new string[] { Utils.PublicKeyToHostname(client.RelayPublicKey) };
                serviceInfo.ServicePorts = config.Services.Select(o => o.RemotePort).ToArray();
                client.SharedRelay.BroadcastServiceInfo(serviceInfo);
                Console.WriteLine("Broadcasted service info");
            }

            Console.ReadLine();
        }
    }
}
