﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libOpenNet.src
{
    public class Config_CachedService
    {
        public byte[] PublicKey { get; set; }
        public string Hostname { get; set; }
        public List<string> RelayEntryPoints { get; set; }
        public List<int> ServicePorts { get; set; }

        public Config_CachedService()
        {

        }
    }
}
