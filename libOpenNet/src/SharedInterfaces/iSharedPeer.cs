﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libOpenNet.src.SharedInterfaces
{
    public interface iSharedPeer
    {
        void SendMessage(string FromHostname, byte[] Data);
    }
}