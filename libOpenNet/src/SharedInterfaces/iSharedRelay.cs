﻿using libOpenNet.src.SharedObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libOpenNet.src.SharedInterfaces
{
    public interface iSharedRelay
    {
        Shared_RelayInfo GetRelayInfo();
        Shared_RelayInfo[] GetAllRelayInfo();
        void BroadcastRelayInfo(Shared_RelayInfo relayInfo);

        void BroadcastServiceInfo(Shared_ServiceInfo serviceInfo);
        Shared_ServiceInfo GetServiceInfo(string Hostname);
        void SendMessage(string Hostname, byte[] Data);
        void SetPublicKey(byte[] PublicKey);
    }
}
