﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libOpenNet.src
{
    public class Config_Service
    {
        public int RemotePort { get; set; }
        public string TargetHost { get; set; }
        public int TargetPort { get; set; }

        public Config_Service()
        {

        }
        public Config_Service(int RemotePort, string TargetHost, int TargetPort)
        {
            this.RemotePort = RemotePort;
            this.TargetHost = TargetHost;
            this.TargetPort = TargetPort;
        }
    }
}
