﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libOpenNet.src.SharedObjects
{
    [ProtoContract]
    public class Shared_ServiceInfo
    {
        [ProtoMember(1)]
        public byte[] PublicKey { get; set; }

        [ProtoMember(2)]
        public string[] RelayEntryPoints { get; set; }

        [ProtoMember(3)]
        public int[] ServicePorts { get; set; }
    }
}
