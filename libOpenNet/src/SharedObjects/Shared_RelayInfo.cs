﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libOpenNet.src.SharedObjects
{
    [ProtoContract]
    public class Shared_RelayInfo
    {
        [ProtoMember(1)]
        public string RelayName { get; set; }

        [ProtoMember(2)]
        public string PublicKey { get; set; }

        [ProtoMember(3)]
        public string RelayIp { get; set; }

        [ProtoMember(4)]
        public int RelayPort { get; set; }

        [ProtoMember(5)]
        public bool IsPrivate { get; set; }

        public Shared_RelayInfo()
        {

        }
    }
}
