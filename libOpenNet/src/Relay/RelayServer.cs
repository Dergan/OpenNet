﻿using libOpenNet.src.Client;
using SecureSocketProtocol3;
using SecureSocketProtocol3.Security.Encryptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace libOpenNet.src.Relay
{
    public class RelayServer : SSPServer
    {
        public Config Config
        {
            get { return (base.serverProperties as RelayServerProperties).Configuration; }
        }

        public RelayServer(string ConfigPath)
            : base(new RelayServerProperties(Config.LoadConfig(ConfigPath)))
        {
            //this.Config = Config.LoadConfig(ConfigPath);

            //ThreadPool.QueueUserWorkItem(RelayNetworkClientThread);
        }

        public override SSPClient GetNewClient()
        {
            return new RelayPeer(this);
        }

        private void RelayNetworkClientThread(object o)
        {
            while (true)
            {
                using (NetworkClient Client = new NetworkClient(Config.ConfigPath))
                {
                    Console.WriteLine("Connected to a relay, sending relay info");

                    Client.SharedRelay.BroadcastRelayInfo(new SharedObjects.Shared_RelayInfo()
                    {
                        IsPrivate = Config.IsPrivate,
                        PublicKey = new RSAEncryption(4096, Config.PrivateKey).PublicRsaXml,
                        RelayIp = "",
                        RelayName = Config.Name,
                        RelayPort = Config.ListenPort
                    });

                    while (Client.Connected)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(30));
                    }
                }

                Thread.Sleep(TimeSpan.FromMinutes(3));
            }
        }

        public RelayPeer FindRelayByHostname(string Hostname)
        {
            return base.GetClients().FirstOrDefault(o => (o as RelayPeer).Hostname == Hostname) as RelayPeer;
        }
    }
}