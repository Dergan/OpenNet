﻿using libOpenNet.src.SharedInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libOpenNet.src.SharedObjects;
using LiteCode.Attributes;
using SecureSocketProtocol3.Security.Encryptions;

namespace libOpenNet.src.Relay
{
    public class SharedRelay : iSharedRelay
    {
        private RelayPeer peer;
        public SharedRelay(RelayPeer peer)
        {
            this.peer = peer;
            this.peer.Shared = this;
        }

        [NoException]
        [RemoteExecution(30000, null)]
        public Shared_RelayInfo GetRelayInfo()
        {
            RSAEncryption rsa = new RSAEncryption(4096, peer.Server.Config.PrivateKey);

            return new Shared_RelayInfo()
            {
                IsPrivate = false,
                PublicKey = rsa.PublicRsaXml,
                RelayName = peer.Server.Config.Name,
                RelayPort = peer.Server.Config.ListenPort
            };
        }

        [NoException]
        [RemoteExecution(30000, null)]
        public Shared_RelayInfo[] GetAllRelayInfo()
        {
            List<Shared_RelayInfo> relays = new List<Shared_RelayInfo>();

            foreach (Config_RelayInfo relay in peer.Server.Config.CachedRelays)
            {
                relays.Add(new Shared_RelayInfo()
                {
                    IsPrivate = relay.IsPrivate,
                    PublicKey = relay.PublicKey,
                    RelayIp = relay.RelayIp,
                    RelayName = relay.Name,
                    RelayPort = relay.RelayPort
                });
            }

            return relays.ToArray();
        }

        [NoException]
        [RemoteExecution(30000, null)]
        public void BroadcastRelayInfo(Shared_RelayInfo relayInfo)
        {
            peer.Server.Config.AddRelay(peer, relayInfo);
        }


        [NoException]
        [RemoteExecution(30000, null)]
        public void BroadcastServiceInfo(Shared_ServiceInfo serviceInfo)
        {
            peer.Server.Config.AddRemoteService(serviceInfo);
            //serviceInfo.
        }

        [NoException]
        [RemoteExecution(30000, null)]
        public Shared_ServiceInfo GetServiceInfo(string Hostname)
        {
            Config_CachedService service = peer.Server.Config.GetRemoteService(Hostname);

            if (service == null)
            {
                //implement here to ask neighbors
                return null;
            }

            return new Shared_ServiceInfo()
            {
                PublicKey = service.PublicKey,
                RelayEntryPoints = service.RelayEntryPoints.ToArray(),
                ServicePorts = service.ServicePorts.ToArray()
            };
        }


        [NoException]
        [UncheckedRemoteExecution()]
        public void SendMessage(string Hostname, byte[] Data)
        {
            RelayPeer TargetPeer = this.peer.Server.FindRelayByHostname(Hostname);

            if (TargetPeer != null)
            {
                TargetPeer.SharedPeer.SendMessage(peer.Hostname, Data);
            }
        }

        [NoException]
        [RemoteExecution(30000, null)]
        public void SetPublicKey(byte[] PublicKey)
        {
            //if(PublicKey == null || PublicKey.Length > 

            this.peer.HostPublicKey = PublicKey;
            this.peer.Hostname = Utils.PublicKeyToHostname(PublicKey);
        }
    }
}
