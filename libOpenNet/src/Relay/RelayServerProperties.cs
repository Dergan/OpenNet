﻿using SecureSocketProtocol3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecureSocketProtocol3.Security.Serialization;
using System.IO;

namespace libOpenNet.src.Relay
{
    public class RelayServerProperties : ServerProperties
    {
        public Config Configuration { get; private set; }

        public RelayServerProperties(Config config)
            : base()
        {
            this.Configuration = config;
        }

        public override TimeSpan ClientTimeConnected
        {
            get { return new TimeSpan(1, 0, 0, 0); }
        }

        public override ISerialization DefaultSerializer
        {
            get { return new ProtobufSerialization(); }
        }

        public override Stream[] KeyFiles
        {
            get
            {
                return new MemoryStream[0];
            }
        }

        public override string ListenIp
        {
            get
            {
                return Configuration.ListenIp;
            }
        }

        public override string ListenIp6
        {
            get
            {
                return Configuration.ListenIp6;
            }
        }

        public override ushort ListenPort
        {
            get
            {
                return (ushort)Configuration.ListenPort;
            }
        }

        public override byte[] NetworkKey
        {
            get
            {
                return new byte[]
                {
                    56, 23, 75, 23, 75, 61, 23, 46, 32, 74, 65, 26, 41, 27, 37, 99,
                    53, 47, 12, 63, 47, 81, 92, 36, 49, 01, 26, 50, 98, 16, 34, 78, 
                    23, 04, 61, 23, 78, 51, 02, 63, 40, 98, 21, 34, 16, 87, 16, 84
                };
            }
        }

        public override bool UseIPv4AndIPv6
        {
            get
            {
                return !String.IsNullOrWhiteSpace(Configuration.ListenIp6);
            }
        }
    }
}
