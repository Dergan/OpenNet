﻿using SecureSocketProtocol3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecureSocketProtocol3.Network;
using SecureSocketProtocol3.Security.DataIntegrity;
using SecureSocketProtocol3.Security.Handshakes;
using SecureSocketProtocol3.Security.Layers;
using LiteCode;
using libOpenNet.src.Client;
using libOpenNet.src.SharedInterfaces;

namespace libOpenNet.src.Relay
{
    public class RelayPeer : SSPClient
    {
        public RelayServer Server { get; private set; }

        public string Hostname { get; set; }
        public byte[] HostPublicKey { get; set; }

        public SharedRelay Shared { get; set; }
        public iSharedPeer SharedPeer { get; set; }

        private LiteCodeClient liteCode;

        public RelayPeer(RelayServer server)
            : base()
        {
            this.Server = server;
        }

        private IDataIntegrityLayer _dataIntegrityLayer;
        public override IDataIntegrityLayer DataIntegrityLayer
        {
            get
            {
                if (_dataIntegrityLayer == null)
                    _dataIntegrityLayer = new HMacLayer(this);
                return _dataIntegrityLayer;
            }
        }

        public override void onApplyHandshakes(HandshakeSystem handshakeSystem)
        {
            SimpleRsaHandshake simpleRsaHandshake = new SimpleRsaHandshake(this, Server.Config.PrivateKey);
            handshakeSystem.AddLayer(simpleRsaHandshake);
        }


        public override void onApplyLayers(LayerSystem layerSystem)
        {
            layerSystem.AddLayer(new AesLayer(base.Connection));
        }

        public override void onBeforeConnect()
        {
            base.RegisterOperationalSocket(new LiteCodeClient(this));
        }

        public override void onConnect()
        {
            liteCode = new LiteCodeClient(this);
            liteCode.Connect();

            this.SharedPeer = liteCode.GetSharedClass<iSharedPeer>("SharedPeer");

            Console.WriteLine("Accepted connection");
        }

        public override void onDisconnect(DisconnectReason Reason)
        {

        }

        public override void onException(Exception ex, ErrorType errorType)
        {

        }

        public override void onOperationalSocket_BeforeConnect(OperationalSocket OPSocket)
        {
            if (OPSocket as LiteCodeClient != null)
            {
                LiteCodeClient liteClient = OPSocket as LiteCodeClient;
                liteClient.ShareClass("SharedRelay", typeof(SharedRelay), false, 1, this);
            }
        }

        public override void onOperationalSocket_Connected(OperationalSocket OPSocket)
        {

        }

        public override void onOperationalSocket_Disconnected(OperationalSocket OPSocket, DisconnectReason Reason)
        {

        }
    }
}