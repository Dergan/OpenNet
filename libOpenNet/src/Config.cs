﻿using libOpenNet.src.Client;
using libOpenNet.src.Relay;
using libOpenNet.src.SharedObjects;
using SecureSocketProtocol3.Security.Encryptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace libOpenNet.src
{
    public class Config
    {
        //The main DNS name to enter the network
        //Please don't change this DNS and help the network
        public static string NETWORK_ENTRY_DNS { get { return "network.somedarknet.org"; } }


        public string Name { get; set; }
        public string ListenIp { get; set; }
        public string ListenIp6 { get; set; }
        public int ListenPort { get; set; }
        public string PrivateKey { get; set; }
        
        public ClientType Type { get; set; }
        public bool IsPrivate { get; set; }
        public string RelayPrivatePassword { get; set; }
        public string IntHostName { get; set; }

        public List<Config_Service> Services { get; set; }
        public List<Config_RelayInfo> CachedRelays { get; set; }
        public List<Config_CachedService> CachedServices { get; set; }

        [NonSerialized]
        public string ConfigPath;

        public Config()
        {
            this.Services = new List<Config_Service>();
            this.CachedRelays = new List<Config_RelayInfo>();
            this.CachedServices = new List<Config_CachedService>();
        }

        private Config(string Path)
             : this()
        {
            this.ConfigPath = Path;
        }

        public static Config LoadConfig(string Path)
        {
            Config config = null;

            if (File.Exists(Path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Config));

                using (FileStream stream = new FileStream(Path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    config = serializer.Deserialize(stream) as Config;
                    config.ConfigPath = Path;
                }
            }

            if (config == null)
            {
                config = new Config(Path);
            }

            if (String.IsNullOrWhiteSpace(config.Name))
            {
                config.Name = "Unnamed";
            }

            if (String.IsNullOrWhiteSpace(config.ListenIp))
            {
                config.ListenIp = "0.0.0.0";
            }

            if (String.IsNullOrWhiteSpace(config.ListenIp6))
            {
                //config.ListenIp6 = "::";
                config.ListenIp6 = "";
            }

            if (config.ListenPort == 0)
            {
                config.ListenPort = 5000;
            }

            if (String.IsNullOrWhiteSpace(config.PrivateKey))
            {
                Console.WriteLine("Generating new RSA-4096 key...");

                RSAEncryption GenPrivateKeyc = new RSAEncryption(4096, true);
                GenPrivateKeyc.GeneratePrivateKey();
                config.PrivateKey = GenPrivateKeyc.PrivateKey;
                config.Save();
            }
            
            //if (String.IsNullOrWhiteSpace(config.IntHostName))
            {
                config.IntHostName = Utils.PublicKeyToHostname(new RSAEncryption(4096, config.PrivateKey).PublicParameters.Value.Modulus);
            }

            if (String.IsNullOrWhiteSpace(config.RelayPrivatePassword))
            {
                config.RelayPrivatePassword = "";
            }

            config.Save();

            return config;
        }

        public void Save()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Config));

            using (FileStream stream = new FileStream(ConfigPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                serializer.Serialize(stream, this);
                stream.SetLength(stream.Position);
                stream.Flush();
            }
        }

        /*public void AddRelay(Shared_RelayInfo relay)
        {
            //remove already existing relay
            foreach (Config_RelayInfo removeConfig in CachedRelays.Where(o => o.RelayIp == relay.RelayIp &&
                                                                              o.RelayPort == relay.RelayPort).ToArray())
            {
                CachedRelays.Remove(removeConfig);
            }

            CachedRelays.Add(new Config_RelayInfo()
            {
                IsPrivate = relay.IsPrivate,
                Name = relay.RelayName,
                PublicKey = relay.PublicKey,
                RelayIp = relay.RelayIp,
                RelayPort = relay.RelayPort
            });
            Save();
        }*/

        public void AddRelay(NetworkClient client, Shared_RelayInfo relay)
        {
            //remove already existing relay
            foreach (Config_RelayInfo removeConfig in CachedRelays.Where(o => o.RelayIp == relay.RelayIp &&
                                                                              o.RelayPort == relay.RelayPort).ToArray())
            {
                CachedRelays.Remove(removeConfig);
            }

            CachedRelays.Add(new Config_RelayInfo()
            {
                IsPrivate = relay.IsPrivate,
                Name = relay.RelayName,
                PublicKey = relay.PublicKey,
                RelayIp = string.IsNullOrWhiteSpace(relay.RelayIp) ? client.ConnectedProperty.ResolvedIp : relay.RelayIp,
                RelayPort = relay.RelayPort
            });
            Save();
        }

        public void AddRelay(RelayPeer peer, Shared_RelayInfo relay)
        {
            //remove already existing relay
            foreach (Config_RelayInfo removeConfig in CachedRelays.Where(o => o.RelayIp == peer.RemoteIp &&
                                                                              o.RelayPort == relay.RelayPort).ToArray())
            {
                CachedRelays.Remove(removeConfig);
            }

            CachedRelays.Add(new Config_RelayInfo()
            {
                IsPrivate = relay.IsPrivate,
                Name = relay.RelayName,
                PublicKey = relay.PublicKey,
                RelayIp = peer.RemoteIp,
                RelayPort = relay.RelayPort
            });
            Save();
        }

        public void AddService(int RemotePort, string TargetHost, int TargetPort)
        {
            Services.Add(new Config_Service(RemotePort, TargetHost, TargetPort));
            Save();
        }

        public void AddRemoteService(Shared_ServiceInfo service)
        {
            //remove already existing relay
            foreach (Config_CachedService removeService in CachedServices.Where(o => o.Hostname.ToLower() == Utils.PublicKeyToHostname(service.PublicKey)).ToArray())
            {
                CachedServices.Remove(removeService);
            }

            CachedServices.Add(new Config_CachedService()
            {
                 Hostname = Utils.PublicKeyToHostname(service.PublicKey),
                 PublicKey = service.PublicKey,
                 RelayEntryPoints = service.RelayEntryPoints.ToList(),
                 ServicePorts = service.ServicePorts.ToList()
            });
            Save();
        }

        public Config_CachedService GetRemoteService(string Hostname)
        {
            return CachedServices.FirstOrDefault(o => o.Hostname.ToLower() == Hostname.ToLower());
        }
    }
}
