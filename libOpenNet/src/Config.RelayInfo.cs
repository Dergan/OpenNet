﻿using SecureSocketProtocol3.Security.Encryptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace libOpenNet.src
{
    public class Config_RelayInfo
    {
        [NonSerialized]
        private string _publicKey;

        public string Name { get; set; }
        public string PublicKey
        {
            get
            {
                return _publicKey;
            }
            set
            {
                _publicKey = value;

                RSAEncryption rsa = new RSAEncryption(4096, value);
                string shafingerPrint = Convert.ToBase64String(rsa.PublicParameters.Value.Modulus);
                shafingerPrint = BitConverter.ToString(SHA512.Create().ComputeHash(ASCIIEncoding.ASCII.GetBytes(shafingerPrint)));
                Sha512Fingerprint = shafingerPrint.Replace('-', ':');
            }
        }
        public string Sha512Fingerprint { get; set; }
        public string RelayIp { get; set; }
        public int RelayPort { get; set; }
        public bool IsPrivate { get; set; }

    }
}