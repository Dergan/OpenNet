﻿using SecureSocketProtocol3.Network.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecureSocketProtocol3;
using SecureSocketProtocol3.Network;
using ProtoBuf;
using SecureSocketProtocol3.Security.Serialization;
using SecureSocketProtocol3.Attributes;

namespace libOpenNet.src.Messages
{
    [ProtoContract]
    [Serialization(typeof(ProtobufSerialization))]
    public class MsgNewKey : IMessage
    {
        [ProtoMember(1)]
        public byte[] Key { get; set; }

        [ProtoMember(2)]
        public byte[] Key_Counter { get; set; }

        [ProtoMember(3)]
        public uint MessageSeed { get; set; }

        [ProtoMember(4)]
        public byte[] HMAC_Key { get; set; }

        public MsgNewKey()
        {

        }
        public MsgNewKey(byte[] Key, byte[] Key_Counter, byte[] HMAC_Key, uint MessageSeed)
        {
            this.Key = Key;
            this.Key_Counter = Key_Counter;
            this.HMAC_Key = HMAC_Key;
            this.MessageSeed = MessageSeed;
        }

        public override void ProcessPayload(SSPClient client, OperationalSocket OpSocket)
        {

        }
    }
}