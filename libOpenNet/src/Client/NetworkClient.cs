﻿using SecureSocketProtocol3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecureSocketProtocol3.Network;
using SecureSocketProtocol3.Security.DataIntegrity;
using SecureSocketProtocol3.Security.Handshakes;
using SecureSocketProtocol3.Security.Layers;
using LiteCode;
using libOpenNet.src.SharedInterfaces;
using libOpenNet.src.SharedObjects;
using SecureSocketProtocol3.Security.Encryptions;
using libOpenNet.src.Relay;
using SecureSocketProtocol3.Utils;
using libOpenNet.src.Messages;

namespace libOpenNet.src.Client
{
    public class NetworkClient : SSPClient
    {
        public string RelayMD5FingerPrint { get; private set; }
        public string RelaySha512FingerPrint { get; private set; }
        public string RelayHostname { get; private set; }

        private LiteCodeClient liteCode;
        public iSharedRelay SharedRelay { get; private set; }

        public byte[] RelayPublicKey { get; private set; }

        public Config Config
        {
            get { return (base.ConnectedProperty as ClientNetworkProperties).Configuration; }
        }

        public delegate void PeerMessageCallback(string FromHostname, byte[] Data);
        public event PeerMessageCallback onPeerMessage;

        public NetworkClient(string ConfigPath)
            : base(new ClientNetworkProperties(ConfigPath))
        {

        }

        private IDataIntegrityLayer _dataIntegrityLayer;
        public override IDataIntegrityLayer DataIntegrityLayer
        {
            get
            {
                if (_dataIntegrityLayer == null)
                    _dataIntegrityLayer = new HMacLayer(this);
                return _dataIntegrityLayer;
            }
        }

        public override void onApplyHandshakes(HandshakeSystem handshakeSystem)
        {
            SimpleRsaHandshake simpleRsaHandshake = new SimpleRsaHandshake(this);
            simpleRsaHandshake.onVerifyFingerPrint += simpleRsaHandshake_onVerifyFingerPrint;
            handshakeSystem.AddLayer(simpleRsaHandshake);
        }

        private bool simpleRsaHandshake_onVerifyFingerPrint(byte[] PublicKey, string Md5FingerPrint, string Sha512FingerPrint)
        {
            Config_RelayInfo cachedRelay = Config.CachedRelays.FirstOrDefault(o => o.RelayIp ==  base.ConnectedProperty.HostIp && 
                                                                                   o.RelayPort == base.ConnectedProperty.Port);

            this.RelayMD5FingerPrint = Md5FingerPrint;
            this.RelaySha512FingerPrint = Sha512FingerPrint;
            this.RelayPublicKey = PublicKey;
            this.RelayHostname = Utils.PublicKeyToHostname(this.RelayPublicKey);

            if (cachedRelay != null && Sha512FingerPrint != cachedRelay.Sha512Fingerprint)
            {
                Console.WriteLine("[Warning] Man in the middle attack ???");
                return false;
            }

            return true;
        }

        public override void onApplyLayers(LayerSystem layerSystem)
        {
            layerSystem.AddLayer(new AesLayer(base.Connection));
        }

        public override void onBeforeConnect()
        {
            base.RegisterOperationalSocket(new LiteCodeClient(this));
        }

        public override void onConnect()
        {
            liteCode = new LiteCodeClient(this);
            liteCode.Connect();
            
            this.SharedRelay = liteCode.GetSharedClass<iSharedRelay>("SharedRelay");

            //let the relay know our public key
            this.SharedRelay.SetPublicKey(new RSAEncryption(4096, Config.PrivateKey).PublicParameters.Value.Modulus);

            Console.WriteLine($"Connected to relay, Sha512 Fingerprint: {RelaySha512FingerPrint}");
        }

        public override void onDisconnect(DisconnectReason Reason)
        {

        }

        public override void onException(Exception ex, ErrorType errorType)
        {

        }

        public override void onOperationalSocket_BeforeConnect(OperationalSocket OPSocket)
        {
            if (OPSocket as LiteCodeClient != null)
            {
                LiteCodeClient liteClient = OPSocket as LiteCodeClient;
                liteClient.ShareClass("SharedPeer", typeof(SharedPeer), false, 1, this);
            }
        }

        public override void onOperationalSocket_Connected(OperationalSocket OPSocket)
        {

        }

        public override void onOperationalSocket_Disconnected(OperationalSocket OPSocket, DisconnectReason Reason)
        {

        }

        public void ConnectToService(string Hostname)
        {
            Shared_ServiceInfo service = SharedRelay.GetServiceInfo(Hostname);

            if (service != null)
            {
                Console.WriteLine($"Resvoled target client hes at {service.RelayEntryPoints.Length} relay(s)");

                bool AlreadyConnectedToRelay = service.RelayEntryPoints.Contains(RelayHostname);

                if (!AlreadyConnectedToRelay)
                {
                    //connect to relay
                    Console.WriteLine("connect to different relay not implemented yet");
                    return;
                }

                bool isRelayPrivate = true;

                if (isRelayPrivate)
                {
                    CircuitMessageQueue msgQueue = new CircuitMessageQueue(this, service);

                    SecureRandom rnd = new SecureRandom();
                    MsgNewKey newKey = new MsgNewKey(rnd.NextBytes(16), rnd.NextBytes(16), rnd.NextBytes(16), rnd.NextUInt());
                    msgQueue.SendMessage(newKey);

                    //msgQueue.Write(new byte[] { 1, 3, 3, 7 }, 0, 4);
                    //byte[] recvMsg = msgQueue.ReadMessage();
                }
                else
                {

                }
            }
        }

        internal void ReceivedMessage(string FromHostname, byte[] Data)
        {



            if (onPeerMessage != null)
            {
                onPeerMessage(FromHostname, Data);
            }
        }
    }
}
