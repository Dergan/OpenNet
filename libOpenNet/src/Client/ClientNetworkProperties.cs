﻿using SecureSocketProtocol3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecureSocketProtocol3.Security.Serialization;
using System.IO;
using SecureSocketProtocol3.Utils;

namespace libOpenNet.src.Client
{
    public class ClientNetworkProperties : ClientProperties
    {
        public Config Configuration { get; private set; }
        private Config_RelayInfo relay;

        public ClientNetworkProperties(string ConfigPath)
            : base()
        {
            this.Configuration = Config.LoadConfig(ConfigPath);

            if (Configuration.CachedRelays.Count > 0)
            {
                //connect to a random relay
                //make it more intelligent later
                //connect within country etc...

                SecureRandom rand = new SecureRandom();
                relay = Configuration.CachedRelays[rand.Next(0, Configuration.CachedRelays.Count)];
            }
        }

        public override int ConnectionTimeout
        {
            get
            {
                return 10000;
            }
        }

        public override ISerialization DefaultSerializer
        {
            get
            {
                return new ProtobufSerialization();
            }
        }

        public override string HostIp
        {
            get
            {
                if (relay != null)
                    return relay.RelayIp;

                return Config.NETWORK_ENTRY_DNS;
            }
        }

        public override ushort Port
        {
            get
            {
                if (relay != null)
                    return (ushort)relay.RelayPort;

                return 5000;
            }
        }

        public override Stream[] KeyFiles
        {
            get
            {
                return new MemoryStream[0];
            }
        }

        public override byte[] NetworkKey
        {
            get
            {
                return new byte[]
                {
                    56, 23, 75, 23, 75, 61, 23, 46, 32, 74, 65, 26, 41, 27, 37, 99,
                    53, 47, 12, 63, 47, 81, 92, 36, 49, 01, 26, 50, 98, 16, 34, 78,
                    23, 04, 61, 23, 78, 51, 02, 63, 40, 98, 21, 34, 16, 87, 16, 84
                };
            }
        }

    }
}
