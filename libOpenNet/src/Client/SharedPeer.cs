﻿using libOpenNet.src.SharedInterfaces;
using LiteCode.Attributes;
using SecureSocketProtocol3.Security.Encryptions;
using SecureSocketProtocol3.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libOpenNet.src.Client
{
    public class SharedPeer : iSharedPeer
    {
        public NetworkClient Client { get; private set; }

        private RSAEncryption RSA;
        private SortedList<string, CircuitInfo> Circuits;

        public SharedPeer(NetworkClient Client)
        {
            this.Client = Client;
            this.Circuits = new SortedList<string, CircuitInfo>();
            this.RSA = new RSAEncryption(4096, Client.Config.PrivateKey);
        }

        [NoException]
        [UncheckedRemoteExecution()]
        public void SendMessage(string FromHostname, byte[] Data)
        {
            if (!Circuits.ContainsKey(FromHostname))
            {
                Circuits.Add(FromHostname, new CircuitInfo(FromHostname, 0));
            }

            CircuitInfo circuit = null;
            if (Circuits.TryGetValue(FromHostname, out circuit))
            {
                Console.WriteLine($"Received from {FromHostname} {BitConverter.ToString(Data)}");

                byte[] decrypted = RSA.Decrypt(Data, 0, Data.Length);

                //circuit.MsgHandler.DeSerialize(

            }


            //Client.SharedRelay.SendMessage(FromHostname, Data);

            //Client.ReceivedMessage(FromHostname, Data);
        }
    }
}
