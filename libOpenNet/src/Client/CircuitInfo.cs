﻿using libOpenNet.src.Messages;
using SecureSocketProtocol3.Network.Messages;
using SecureSocketProtocol3.Security.Layers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libOpenNet.src.Client
{
    public class CircuitInfo
    {
        public string FromHostname { get; private set; }
        public MessageHandler MsgHandler { get; private set; }
        public AesCtrLayer AES { get; private set; }

        public CircuitInfo(string FromHostname, uint MessageSeed)
        {
            this.FromHostname = FromHostname;
            this.MsgHandler = new MessageHandler(MessageSeed, null);
            this.MsgHandler.AddMessage(typeof(MsgNewKey), "MESSAGE_NEW_KEY");
        }

        public void ApplyKey(MsgNewKey key)
        {
            this.AES = new AesCtrLayer(key.Key, key.Key_Counter);
            this.MsgHandler = new MessageHandler(key.MessageSeed, null);

        }
    }
}
