﻿using libOpenNet.src.SharedObjects;
using SecureSocketProtocol3.Network.Messages;
using SecureSocketProtocol3.Security.Encryptions;
using SecureSocketProtocol3.Security.Layers;
using SecureSocketProtocol3.Security.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libOpenNet.src.Client
{
    public class CircuitMessageQueue : IDisposable
    {
        private Queue<byte[]> messages;
        private NetworkClient client;
        private string TargetHostname;
        private RSAEncryption RSA;
        private AesCtrLayer AES;
        private MessageHandler msgHandler;
        private ProtobufSerialization serializer;

        public CircuitMessageQueue()
        {
            this.messages = new Queue<byte[]>();
            this.msgHandler = new MessageHandler(0, null);
            this.serializer = new ProtobufSerialization();
            //this.AES = new AesCtrLayer(
        }

        public CircuitMessageQueue(NetworkClient client, string TargetHostname)
            : this()
        {
            this.client = client;
            this.TargetHostname = TargetHostname;
            this.client.onPeerMessage += SharedPeer_onPeerMessage;
        }
        public CircuitMessageQueue(NetworkClient client, Shared_ServiceInfo service)
            : this()
        {
            this.client = client;
            this.TargetHostname = Utils.PublicKeyToHostname(service.PublicKey);
            this.RSA = new RSAEncryption(4096, service.PublicKey, new byte[] { 1, 0, 1 });
            this.client.onPeerMessage += SharedPeer_onPeerMessage;
        }

        private void SharedPeer_onPeerMessage(string FromHostname, byte[] Data)
        {
            if (FromHostname == TargetHostname)
            {
                messages.Enqueue(Data);
            }
        }

        public void Dispose()
        {

        }

        public IMessage ReadMessage()
        {
            /*lock (messages)
            {
                if (messages.Count > 0)
                {
                    byte[] data = messages.Dequeue();

                    if (AES != null)
                    {
                        byte[] decrypted = new byte[0];
                        int offset = 0;
                        int len = 0;
                        AES.RemoveLayer(data, 0, data.Length, ref decrypted, ref offset, ref len);
                        return decrypted;
                    }

                    return RSA.Decrypt(data, 0, data.Length);
                }
            }*/

            while (messages.Count == 0)
                System.Threading.Thread.Sleep(100);

            //fix this shit k
            lock (messages)
            {
                if (messages.Count > 0)
                {
                    byte[] data = messages.Dequeue();

                    if (AES != null)
                    {
                        byte[] decrypted = new byte[0];
                        int offset = 0;
                        int len = 0;
                        AES.RemoveLayer(data, 0, data.Length, ref decrypted, ref offset, ref len);
                        return null;
                    }

                    return null;// RSA.Decrypt(data, 0, data.Length);
                }
            }
            return null;
        }

        public void SendMessage(IMessage Message)
        {
            byte[] data = serializer.Serialize(Message);

            byte[] encrypted = new byte[0];

            if (AES != null)
            {
                int offset = 0;
                int len = 0;
                AES.ApplyLayer(data, 0, data.Length, ref encrypted, ref offset, ref len);
            }
            else if (RSA != null)
            {
                encrypted = RSA.Encrypt(data, 0, data.Length);
            }

            client.SharedRelay.SendMessage(TargetHostname, encrypted);

            /*if (buffer.Length != count || offset != 0)
            {
                byte[] data = new byte[count];
                Array.Copy(buffer, offset, data, 0, count);
                client.SharedRelay.SendMessage(TargetHostname, data);
            }
            else
            {
                client.SharedRelay.SendMessage(TargetHostname, buffer);
            }*/
        }
    }
}