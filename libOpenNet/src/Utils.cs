﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace libOpenNet.src
{
    public class Utils
    {


        public static string PublicKeyToHostname(byte[] Modulus)
        {
            //convert the 4096-bit modulus to SHA256
            byte[] hostname = SHA256.Create().ComputeHash(Modulus);

            string hostnameStr = Convert.ToBase64String(hostname);

            //remove "non-readable" characters
            //remove "zero" because it's confusing... 0 O
            string removeCharacters = "0+/=";

            for (int i = 0; i < removeCharacters.Length; i++)
            {
                hostnameStr = hostnameStr.Replace(removeCharacters[i].ToString(), "");
            }

            return hostnameStr.ToLower();
        }
    }
}
