﻿using libOpenNet.src;
using libOpenNet.src.Relay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenNet_PrivateRelay
{
    class Program
    {
        static string ConfigPath = "./config.xml";

        static void Main(string[] args)
        {
            Console.Title = "Private Relay";
            Config config = Config.LoadConfig(ConfigPath);

            RelayServer relay = new RelayServer(ConfigPath);

            Console.WriteLine("Started Private Relay");
            Console.ReadLine();
        }
    }
}
