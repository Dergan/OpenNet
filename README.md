# OpenNet

At this moment the project is very alpha, don't expect stuff to work as expected

OpenNet at this very moment is able to send messages from A to B over a relay but that's it

No Socks/LocalPorts or anything yet, you could however host a test service

Soon you will be able to reach the very first OpenNet service which will be hosted at this address: aovspwnvwctgqeucqbclxknitdtma8enkyqaboye

## Upcoming features
- OpenNet is a isolated network (can't access the ClearNet websites)
- Host services through 1 hop to hide your own Ip Address and to keep performance high
- Create private relays for your own need (key,password protected)
- End-To-End encrypted provided by TLS1.2
- Certificates are RSA-4096bit
- Application Pools, LoadBalancer-Like inside the network for high performance applications

# Libraries Required:
https://github.com/AnguisCaptor/SecureSocketProtocol

https://github.com/AnguisCaptor/LiteCode
